//Library Includes
#include <SFML/Graphics.hpp> // Library for creating and managing SFML windows.
#include <SFML/Audio.hpp> // Library for using audio in SFML
#include <string> // Library for manipulating strings of text
#include <vector> // To allow us the use of vector collections
#include "Item.h" // Include our own class definitions
#include "Player.h" // Include our own class definitions
#include <cstdlib> // Random numbers
#include <time.h>

//The main() Function - entry point for the program
int main()
{
	// Declare the SFML window, called gameWindow 
	sf::RenderWindow gameWindow;

	// Set up the SFML window and pass in the dimmensions and name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Whack-a-Mole", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	//Player Sprite
	// Declare an image, called playerTexture
	sf::Texture playerTexture;

	// Assign the player.png from the file to the playerTexture
	playerTexture.loadFromFile("Assets/Graphics/player.png");

	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());

	// Hole sprites
	// Load the hole texture 
	sf::Texture holeTexture;
	holeTexture.loadFromFile("Assets/Graphics/hole.png");

	// Create the vector to hold our items
	std::vector<sf::Sprite> holes;

	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));
	holes.push_back(sf::Sprite(holeTexture));

	// Postion the holes manually
	holes[0].setPosition(gameWindow.getSize().x / 2 - holes[4].getLocalBounds().width / 2 - 200,
		gameWindow.getSize().y / 2 - holes[4].getLocalBounds().height / 2 - 200);

	holes[1].setPosition(gameWindow.getSize().x / 2 - holes[4].getLocalBounds().width / 2,
		gameWindow.getSize().y / 2 - holes[4].getLocalBounds().height / 2 - 200);

	holes[2].setPosition(gameWindow.getSize().x / 2 - holes[4].getLocalBounds().width / 2 + 200,
		gameWindow.getSize().y / 2 - holes[4].getLocalBounds().height / 2 - 200);

	holes[3].setPosition(gameWindow.getSize().x / 2 - holes[3].getLocalBounds().width / 2 - 200,
		gameWindow.getSize().y / 2 - holes[3].getLocalBounds().height / 2);

	holes[4].setPosition(gameWindow.getSize().x / 2 - holes[4].getLocalBounds().width / 2,
		gameWindow.getSize().y / 2 - holes[4].getLocalBounds().height / 2);

	holes[5].setPosition(gameWindow.getSize().x / 2 - holes[5].getLocalBounds().width / 2 + 200,
		gameWindow.getSize().y / 2 - holes[5].getLocalBounds().height / 2);

	holes[6].setPosition(gameWindow.getSize().x / 2 - holes[4].getLocalBounds().width / 2 - 200,
		gameWindow.getSize().y / 2 - holes[4].getLocalBounds().height / 2 + 200);

	holes[7].setPosition(gameWindow.getSize().x / 2 - holes[7].getLocalBounds().width / 2,
		gameWindow.getSize().y / 2 - holes[7].getLocalBounds().height / 2 + 200);

	holes[8].setPosition(gameWindow.getSize().x / 2 - holes[8].getLocalBounds().width / 2 + 200,
		gameWindow.getSize().y / 2 - holes[8].getLocalBounds().height / 2 + 200);

	// Game Music
	// Declare a music variable, called gameMusic
	sf::Music gameMusic;

	// Open the music.ogg file and play it
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	// Game Font
	// Declare a Font variable, called gameFont
	sf::Font gameFont;

	// Load the mainFont.ttf from the file
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Title Text
	// Declare a Text variable, called titletext
	sf::Text titleText;

	// Set titleText's font to gameFont
	titleText.setFont(gameFont);

	// Set the string that will display for titleText
	titleText.setString("Whack-a-Mole");

	// Set the different parameters of titleText
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::Red);
	titleText.setStyle(sf::Text::Bold);
	titleText.setPosition(
		gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Score
	// Declare an integer variable, called score and initalise it to 0
	int score = 0;

	// Declare a Text variable, called scoreText
	sf::Text scoreText;

	// Set scoreText's font to gameFont
	scoreText.setFont(gameFont);

	// Set the string that will display for scoreText as well as the integer value of score
	scoreText.setString("Score: " + std::to_string(score));

	// Set the different parameters of scoreText
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::Black);
	scoreText.setPosition(30, 30);

	// Timer
	// Declare a Text variable, called timerText
	sf::Text timerText;

	// Set timerText's font to gameFont
	timerText.setFont(gameFont);

	// Set the string that will display for timerText
	timerText.setString("Time Remnaining: 0");

	// Set the different parameters of timerText
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::Black);
	timerText.setPosition(
		gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);

	// Declare a Time variable, called timeLimit and set it to 60 seconds
	sf::Time timeLimit = sf::seconds(60.0f);

	// Declare a Time varibale, called timeRemaining and set it to timeLimit
	sf::Time timeRemaining = timeLimit;

	// Game Clock
	// Declare a Clock variable, called gameClock
	sf::Clock gameClock;

	// Seed the random number generator
	srand(time(NULL));

	// Items
	// Load all three textures that will be used by our items
	std::vector<sf::Texture> itemTextures;

	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());

	itemTextures[0].loadFromFile("Assets/Graphics/coinBronze.png");
	itemTextures[1].loadFromFile("Assets/Graphics/coinSilver.png");
	itemTextures[2].loadFromFile("Assets/Graphics/coinGold.png");

	// Create the vector to hold our items
	std::vector<Item> items;

	// Load up a few starting items
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));

	//Create a time value to store the total time between each item spawn
	sf::Time itemSpawnDuration = sf::seconds(2.0f);

	// Create a timer to store the time remaining for our game
	sf::Time itemSpawnRemaining = itemSpawnDuration;

	sf::Vector2f playerVelocity(0.0f, 0.0f);

	float speed = 100.0f;

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");

	// Set up a Sound object to play the sound later and associate it with the sound buffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load the victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");

	// Setup a Sound object to play the sound later and associate it with the soundBuffer
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game over text
	// Declare a text variable called gameOverText to hold the game over display
	sf::Text gameOverText;

	//Set the font our text should use
	gameOverText.setFont(gameFont);

	// Set the string that will be displayed by this text object
	gameOverText.setString("GAME OVER");

	//Set the string's parameters
	gameOverText.setCharacterSize(72);
	gameOverText.setFillColor(sf::Color::Red);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(
		gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	// Reset/Quit Text
	// Declare a text variable called resetGameText to hold in the game over display
	sf::Text resetGameText;

	// Set the font our text should use
	resetGameText.setFont(gameFont);

	// Set the string that will be displayed by this text object
	resetGameText.setString("Press R to restart or Press Q to quit");

	// Set the string's parameters
	resetGameText.setCharacterSize(24);
	resetGameText.setFillColor(sf::Color::Black);
	resetGameText.setStyle(sf::Text::Bold);
	resetGameText.setPosition(
		gameWindow.getSize().x / 2 - resetGameText.getLocalBounds().width / 2, 300);

	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game Loop
	// Repeat as long as the window is open

	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------

		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;

		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This will section repeat for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		}// End of polling loop

		// Player keybind input
		playerObject.Input();

		// Check if we should reset the game
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			// Reset the game
			score = 0;
			timeRemaining = timeLimit;
			items.clear();
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
			playerObject.Reset(gameWindow.getSize());
		}

		// Check if we should quit the game
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			// Quit the game
			return 0;
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------

		// Get the time passed since the last frame and restart the game clock
		sf::Time frameTime = gameClock.restart();

		// Update timeRemaining based on how much time has passed since the last frame
		timeRemaining = timeRemaining - frameTime;

		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let time go lower than 0
			timeRemaining = sf::seconds(0);

			// Perform these actions only once when the game first ends
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
				gameOver = true;

				//Stop the main music from playing
				gameMusic.stop();

				// Play the victory sound
				victorySound.play();
			}
		}

		// Update timerText based on timeRemaining
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

		// Update the scoreText string to match the score variable
		scoreText.setString("Score: " + std::to_string(score));

		// Only perform this update logic if the game is still running
		if (!gameOver)
		{
			// Update our item spawn time remaining based on how much time passed last frame
			itemSpawnRemaining = itemSpawnRemaining - frameTime;

			// Check if time reamining to next spawn has reached 0
			if (itemSpawnRemaining <= sf::seconds(0.0f))
			{
				// Time to spawn a new item
				items.push_back(Item(itemTextures, gameWindow.getSize()));

				// Reset time remaining to full duration
				itemSpawnRemaining = itemSpawnDuration;
			}

			// Move the player
			playerObject.Update(frameTime);

			// Check for collisions
			for (int i = items.size() - 1; i >= 0; --i)
			{
				sf::FloatRect itemBounds = items[i].sprite.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();

				if (itemBounds.intersects(playerBounds))
				{
					// Add the item's value to the score
					score += items[i].pointsValue;

					// Remove the item from the vector
					items.erase(items.begin() + i);

					// Increase the player's movement speed
					playerObject.speed += 20.0f;

					// Play the pickup sound
					pickupSound.play();
				}
			}
		}

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------

		// Clear the window to a single colour
		gameWindow.clear(sf::Color::White);

		// Draw everything to the window
		gameWindow.draw(titleText);
		gameWindow.draw(scoreText);
		gameWindow.draw(timerText);

		// Only draw these items if the game has not ended
		if (!gameOver)
		{
			// Draw the player
			gameWindow.draw(playerObject.sprite);

			// Draw all our items
			for (int i = 0; i < items.size(); ++i)
			{
				gameWindow.draw(items[i].sprite);
			}

			// Draw the holes
			for (int i = 0; i < holes.size(); ++i)
			{
				gameWindow.draw(holes[i]);
			}
		}

		// Only draw these items if the game has ended:
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
			gameWindow.draw(resetGameText);
		}

		// Display the window contents on the screen 
		gameWindow.display();

	}// End of Game Loop

	return 0;
}// End of main() function