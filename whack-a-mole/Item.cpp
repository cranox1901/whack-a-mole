#include "Item.h"
#include <cstdlib>

Item::Item(std::vector<sf::Texture>& itemTextures, sf::Vector2u screenSize)
{
	// Choose a random number that will dictate which texture to use
	int chosenIndex = rand() % itemTextures.size();

	// Use our random number to pick which texture to use, and give that texture to the sprite
	sprite.setTexture(itemTextures[chosenIndex]);

	// Use our random number to generate the amount of points our Item should be worth :
	pointsValue = chosenIndex * 100 + 100;

	// Choose random x and y positions based on screen size, reduced by the size of the texture to avoid spawning partially off screen.
	int positionX = rand() % (screenSize.x - itemTextures[chosenIndex].getSize().x);
	int positionY = rand() % (screenSize.y - itemTextures[chosenIndex].getSize().y);

	// Set our sprite position based on these random positions
	sprite.setPosition(positionX, positionY);
}