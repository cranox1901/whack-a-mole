#pragma once
#include <SFML/Graphics.hpp> // Library needed for using sprites, textures and fonts
#include <vector> // Library for handling collections of objects

class Player
{
public: // Access level (to be discussed later)

// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u screenSize);

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);

	// Variables used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
};