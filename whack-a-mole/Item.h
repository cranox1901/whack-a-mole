#pragma once
#include <SFML/Graphics.hpp> // Library needed for using sprites, textures and fonts
#include <vector> // Library for handling collections of objects

// definition of Item class
class Item
{
public: // Access level (to be discussed later)

// Constructor
	Item(std::vector<sf::Texture>& itemTextures, sf::Vector2u screenSize);

	// Variables used by this class
	sf::Sprite sprite;
	int pointsValue;
};